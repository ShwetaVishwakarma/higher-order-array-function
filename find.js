module.exports.find = (elements, cb) => {

    for(let i=0;i<elements.length;i++){
       let res = cb(elements[i])
       if(res){
           return elements[i]
       }
   }
   return undefined;
}